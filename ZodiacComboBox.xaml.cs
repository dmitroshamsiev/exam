﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExamShamsievDima
{
    /// <summary>
    /// Interaction logic for ZodiacComboBox.xaml
    /// </summary>
    public partial class ZodiacComboBox : UserControl
    {

        public DateTime LowerBound { get; set; }

        public DateTime UpperBound { get; set; }

        /// <summary>
        /// Removes selection.
        /// </summary>
        public void Clear()
        {
            Zodiacs.SelectedIndex = -1;
        }

        public ZodiacComboBox()
        {
            InitializeComponent();
        }

        private void ComboBox_SelectionChanged( object sender, SelectionChangedEventArgs e )
        {
            if ( Zodiacs.SelectedItem == null )
            {
                LowerBound = default(DateTime);
                UpperBound = default(DateTime);
                return;
            }

            if ( Zodiacs.SelectedItem == Aries )
            {
                LowerBound = new DateTime(1, 4,19);
                UpperBound = new DateTime(1, 5,13);
                return;
            }

            if ( Zodiacs.SelectedItem == Taurus )
            {
                LowerBound = new DateTime(1, 5,14);
                UpperBound = new DateTime(1, 6,19);
                return;
            }

            if ( Zodiacs.SelectedItem == Gemini )
            {
                LowerBound = new DateTime(1, 6,20);
                UpperBound = new DateTime( 1, 7, 20 );
                return;
            }

            if ( Zodiacs.SelectedItem == Cancer )
            {
                LowerBound = new DateTime( 1, 7, 21 );
                UpperBound = new DateTime( 1, 8, 9 );
                return;
            }

            if ( Zodiacs.SelectedItem == Leo )
            {
                LowerBound = new DateTime( 1, 8, 10 );
                UpperBound = new DateTime( 1, 9, 15 );
                return;
            }

            if ( Zodiacs.SelectedItem == Virgo )
            {
                LowerBound = new DateTime( 1, 9, 16 );
                UpperBound = new DateTime( 1, 10, 30 );
                return;
            }

            if ( Zodiacs.SelectedItem == Libra )
            {
                LowerBound = new DateTime( 1, 10, 31 );
                UpperBound = new DateTime( 1, 11, 22 );
                return;
            }

            if ( Zodiacs.SelectedItem == Scorpio )
            {
                LowerBound = new DateTime( 1, 11, 23 );
                UpperBound = new DateTime( 1, 12, 17 );
                return;
            }

            if ( Zodiacs.SelectedItem == Sagittarius )
            {
                LowerBound = new DateTime( 1, 12, 18 );
                UpperBound = new DateTime( 1, 1, 18 );
                return;
            }

            if ( Zodiacs.SelectedItem == Capricorn )
            {
                LowerBound = new DateTime( 1, 1, 19 );
                UpperBound = new DateTime( 1, 2, 15 );
                return;
            }
            if ( Zodiacs.SelectedItem == Aquarius )
            {
                LowerBound = new DateTime( 1, 2, 16 );
                UpperBound = new DateTime( 1, 3, 11 );
                return;
            }
            if ( Zodiacs.SelectedItem == Capricorn )
            {
                LowerBound = new DateTime( 1, 3, 12 );
                UpperBound = new DateTime( 1, 4, 18 );
                return;
            }

        }
    }
}
