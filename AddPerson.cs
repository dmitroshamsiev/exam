﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExamShamsievDima
{
    public partial class AddPerson : Form
    {
        /// <summary>
        /// Editable person
        /// </summary>
        Person _person;

        Person _tempPerson;

        bool _firstNameValid;
        bool _lastNameValid;
        bool _birthDateValid;
        bool _genderValid;
        bool _countryValid;
        bool _cityValid;
        bool _hairValid;
        bool _eyesValid;
        bool _heightValid;
        bool _weightValid;

        /// <summary>
        /// Indicates whether the Person instance is valid.
        /// </summary>
        public bool Valide
        {
            get
            {
                return
                    _firstNameValid &&
                    _lastNameValid &&
                    _birthDateValid &&
                    _genderValid &&
                    _countryValid &&
                    _cityValid &&
                    _hairValid &
                    _eyesValid &&
                    _heightValid &&
                    _weightValid;
            }
        }

        /// <summary>
        /// Windows for adding or editing a Person instance.
        /// </summary>
        /// <param name="person">Person instance.</param>
        public AddPerson( Person person, bool edit = false )
        {
            InitializeComponent();

            _person = person;
            _tempPerson = new Person();

            // Add genders into combobox
            GenderBox.Items.Add( Gender.Male );
            GenderBox.Items.Add( Gender.Female );

            if ( edit )
            {
                FirstNameTxt.Text = person.FirstName;
                LastNameTxt.Text = person.LastName;
                BirthDateTxt.Text = person.BirthDate.ToString( "dd/MM/yyyy" );
                GenderBox.SelectedItem = person.Gender;
                WeightTxt.Text = person.Weght.ToString();
                HeightTxt.Text = person.Height.ToString();
                CountryTxt.Text = person.Country;
                CityTxt.Text = person.City;
                HairTxt.Text = person.HairColor;
                EyesTxt.Text = person.EyesColor;

                ValidateFirstName();
                ValidateLastName();
                ValidateBirthDate();
                ValidateGender();
                ValidateHeight();
                ValidateWeight();
                ValidateCountry();
                ValidateCity();
                ValidateHair();
                ValidateEyes();
            }
        }


        /// <summary>
        /// Validates first name
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FirstNameTxt_Leave( object sender, EventArgs e )
        {
            ValidateFirstName();
        }

        private void ValidateFirstName()
        {
            var txt = FirstNameTxt;
            if ( !string.IsNullOrWhiteSpace( txt.Text ) )
            {
                _tempPerson.FirstName = txt.Text;
                _firstNameValid = true;
                txt.BackColor = Color.Honeydew;
                return;
            }

            _firstNameValid = false;
            txt.BackColor = Color.MistyRose;
        }

        /// <summary>
        /// Validates birth date
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BirthDateTxt_Leave( object sender, EventArgs e )
        {
            ValidateBirthDate();
        }

        private void ValidateBirthDate()
        {
            var txt = BirthDateTxt;
            DateTime bd;
            if ( !string.IsNullOrWhiteSpace( txt.Text ) &&
                DateTime.TryParseExact( txt.Text, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out bd ) )
            {
                // date is valid
                _tempPerson.BirthDate = bd;
                _birthDateValid = true;
                txt.BackColor = Color.Honeydew;
                return;
            }

            _birthDateValid = false;
            txt.BackColor = Color.MistyRose;
        }

        private void LastNameTxt_Leave( object sender, EventArgs e )
        {
            ValidateLastName();
        }

        private void ValidateLastName()
        {
            var txt = LastNameTxt;
            if ( !string.IsNullOrWhiteSpace( txt.Text ) )
            {
                // is valid
                _tempPerson.LastName = txt.Text;
                txt.BackColor = Color.Honeydew;
                _lastNameValid = true;
                return;
            }

            _lastNameValid = false;
            txt.BackColor = Color.MistyRose;
        }

        private void GenderBox_Leave( object sender, EventArgs e )
        {
            ValidateGender();
        }

        private void ValidateGender()
        {
            var combo = GenderBox;
            if ( null != combo.SelectedItem && combo.SelectedItem is Gender )
            {
                // is valid
                _tempPerson.Gender = ( Gender )combo.SelectedItem;
                _genderValid = true;
                combo.BackColor = Color.Honeydew;
                return;
            }

            _genderValid = false;
            combo.BackColor = Color.MistyRose;
        }

        private void HeightTxt_Leave( object sender, EventArgs e )
        {
            ValidateHeight();
        }

        private void ValidateHeight()
        {
            var txt = HeightTxt;
            int h;
            if ( !string.IsNullOrWhiteSpace( txt.Text ) && Int32.TryParse( Regex.Match( txt.Text, "\\d+" ).Value, out h ) )
            {
                // is valid
                _tempPerson.Height = h;
                _heightValid = true;
                txt.BackColor = Color.Honeydew;
                return;
            }

            _heightValid = false;
            txt.BackColor = Color.MistyRose;
        }

        private void WeightTxt_Leave( object sender, EventArgs e )
        {
            ValidateWeight();
        }

        private void ValidateWeight()
        {
            var txt = WeightTxt;
            int w;
            if ( !string.IsNullOrWhiteSpace( txt.Text ) && Int32.TryParse( Regex.Match( txt.Text, "\\d+" ).Value, out w ) )
            {
                // is valid
                _tempPerson.Weght = w;
                _weightValid = true;
                txt.BackColor = Color.Honeydew;
                return;
            }

            _weightValid = false;
            txt.BackColor = Color.MistyRose;
        }

        private void CountryTxt_Leave( object sender, EventArgs e )
        {
            ValidateCountry();
        }

        private void ValidateCountry()
        {
            var txt = CountryTxt;
            if ( !string.IsNullOrWhiteSpace( txt.Text ) )
            {
                // is valid
                _tempPerson.Country = txt.Text;
                txt.BackColor = Color.Honeydew;
                _countryValid = true;
                return;
            }

            _countryValid = false;
            txt.BackColor = Color.MistyRose;
        }

        private void CityTxt_Leave( object sender, EventArgs e )
        {
            ValidateCity();
        }

        private void ValidateCity()
        {
            var txt = CityTxt;
            if ( !string.IsNullOrWhiteSpace( txt.Text ) )
            {
                // is valid
                _tempPerson.City = txt.Text;
                txt.BackColor = Color.Honeydew;
                _cityValid = true;
                return;
            }

            _cityValid = false;
            txt.BackColor = Color.MistyRose;
        }

        private void HairTxt_Leave( object sender, EventArgs e )
        {
            ValidateHair();
        }

        private void ValidateHair()
        {
            var txt = HairTxt;
            if ( !string.IsNullOrWhiteSpace( txt.Text ) )
            {
                // is valid
                _tempPerson.HairColor = txt.Text;
                txt.BackColor = Color.Honeydew;
                _hairValid = true;
                return;
            }

            _hairValid = false;
            txt.BackColor = Color.MistyRose;
        }

        private void EyesTxt_Leave( object sender, EventArgs e )
        {
            ValidateEyes();
        }

        private void ValidateEyes()
        {
            var txt = EyesTxt;
            if ( !string.IsNullOrWhiteSpace( txt.Text ) )
            {
                // is valid
                _tempPerson.EyesColor = txt.Text;
                txt.BackColor = Color.Honeydew;
                _eyesValid = true;
                return;
            }

            _eyesValid = false;
            txt.BackColor = Color.MistyRose;
        }

        private void button1_Click_1( object sender, EventArgs e )
        {
            if ( !Valide )
                return;

            _person.FirstName = _tempPerson.FirstName;
            _person.LastName = _tempPerson.LastName;
            _person.Gender = _tempPerson.Gender;
            _person.Country = _tempPerson.Country;
            _person.City = _tempPerson.City;
            _person.HairColor = _tempPerson.HairColor;
            _person.EyesColor = _tempPerson.EyesColor;
            _person.Height = _tempPerson.Height;
            _person.Weght = _tempPerson.Weght;
            _person.BirthDate = _tempPerson.BirthDate;

            Close();
        }

    }
}
