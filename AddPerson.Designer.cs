﻿namespace ExamShamsievDima
{
    partial class AddPerson
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.FirstNameTxt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.GenderBox = new System.Windows.Forms.ComboBox();
            this.LastNameTxt = new System.Windows.Forms.TextBox();
            this.CountryTxt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.CityTxt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.EyesTxt = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.HairTxt = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.BirthDateTxt = new System.Windows.Forms.MaskedTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.ValidationError = new System.Windows.Forms.Label();
            this.HeightTxt = new System.Windows.Forms.MaskedTextBox();
            this.WeightTxt = new System.Windows.Forms.MaskedTextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "First Name";
            // 
            // FirstNameTxt
            // 
            this.FirstNameTxt.Location = new System.Drawing.Point(16, 68);
            this.FirstNameTxt.Name = "FirstNameTxt";
            this.FirstNameTxt.Size = new System.Drawing.Size(155, 20);
            this.FirstNameTxt.TabIndex = 1;
            this.FirstNameTxt.Leave += new System.EventHandler(this.FirstNameTxt_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(187, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Last Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 24;
            this.label2.Text = "Gender";
            // 
            // GenderBox
            // 
            this.GenderBox.FormattingEnabled = true;
            this.GenderBox.Location = new System.Drawing.Point(16, 112);
            this.GenderBox.Name = "GenderBox";
            this.GenderBox.Size = new System.Drawing.Size(155, 21);
            this.GenderBox.TabIndex = 3;
            this.GenderBox.Leave += new System.EventHandler(this.GenderBox_Leave);
            // 
            // LastNameTxt
            // 
            this.LastNameTxt.Location = new System.Drawing.Point(190, 68);
            this.LastNameTxt.Name = "LastNameTxt";
            this.LastNameTxt.Size = new System.Drawing.Size(155, 20);
            this.LastNameTxt.TabIndex = 2;
            this.LastNameTxt.Leave += new System.EventHandler(this.LastNameTxt_Leave);
            // 
            // CountryTxt
            // 
            this.CountryTxt.Location = new System.Drawing.Point(16, 154);
            this.CountryTxt.Name = "CountryTxt";
            this.CountryTxt.Size = new System.Drawing.Size(155, 20);
            this.CountryTxt.TabIndex = 6;
            this.CountryTxt.Leave += new System.EventHandler(this.CountryTxt_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 137);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 29;
            this.label3.Text = "Country";
            // 
            // CityTxt
            // 
            this.CityTxt.Location = new System.Drawing.Point(190, 154);
            this.CityTxt.Name = "CityTxt";
            this.CityTxt.Size = new System.Drawing.Size(155, 20);
            this.CityTxt.TabIndex = 7;
            this.CityTxt.Leave += new System.EventHandler(this.CityTxt_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(187, 137);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(24, 13);
            this.label5.TabIndex = 31;
            this.label5.Text = "City";
            // 
            // EyesTxt
            // 
            this.EyesTxt.Location = new System.Drawing.Point(190, 200);
            this.EyesTxt.Name = "EyesTxt";
            this.EyesTxt.Size = new System.Drawing.Size(155, 20);
            this.EyesTxt.TabIndex = 9;
            this.EyesTxt.Leave += new System.EventHandler(this.EyesTxt_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(187, 183);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 13);
            this.label6.TabIndex = 35;
            this.label6.Text = "Eyes Color";
            // 
            // HairTxt
            // 
            this.HairTxt.Location = new System.Drawing.Point(16, 200);
            this.HairTxt.Name = "HairTxt";
            this.HairTxt.Size = new System.Drawing.Size(155, 20);
            this.HairTxt.TabIndex = 8;
            this.HairTxt.Leave += new System.EventHandler(this.HairTxt_Leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 183);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 33;
            this.label7.Text = "Hair Color";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(187, 97);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 37;
            this.label8.Text = "Height";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(274, 97);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 13);
            this.label9.TabIndex = 38;
            this.label9.Text = "Weight";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 13);
            this.label10.TabIndex = 39;
            this.label10.Text = "Birth Date";
            // 
            // BirthDateTxt
            // 
            this.BirthDateTxt.BackColor = System.Drawing.SystemColors.Window;
            this.BirthDateTxt.Location = new System.Drawing.Point(16, 25);
            this.BirthDateTxt.Mask = "00/00/0000";
            this.BirthDateTxt.Name = "BirthDateTxt";
            this.BirthDateTxt.Size = new System.Drawing.Size(155, 20);
            this.BirthDateTxt.TabIndex = 0;
            this.BirthDateTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.BirthDateTxt.ValidatingType = typeof(System.DateTime);
            this.BirthDateTxt.Leave += new System.EventHandler(this.BirthDateTxt_Leave);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(270, 238);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Add/Edit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // ValidationError
            // 
            this.ValidationError.AutoSize = true;
            this.ValidationError.ForeColor = System.Drawing.Color.Red;
            this.ValidationError.Location = new System.Drawing.Point(16, 247);
            this.ValidationError.Name = "ValidationError";
            this.ValidationError.Size = new System.Drawing.Size(0, 13);
            this.ValidationError.TabIndex = 42;
            // 
            // HeightTxt
            // 
            this.HeightTxt.Location = new System.Drawing.Point(190, 112);
            this.HeightTxt.Mask = "000 cm";
            this.HeightTxt.Name = "HeightTxt";
            this.HeightTxt.Size = new System.Drawing.Size(69, 20);
            this.HeightTxt.TabIndex = 4;
            this.HeightTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.HeightTxt.Leave += new System.EventHandler(this.HeightTxt_Leave);
            // 
            // WeightTxt
            // 
            this.WeightTxt.Location = new System.Drawing.Point(276, 112);
            this.WeightTxt.Mask = "009 kg";
            this.WeightTxt.Name = "WeightTxt";
            this.WeightTxt.Size = new System.Drawing.Size(69, 20);
            this.WeightTxt.TabIndex = 5;
            this.WeightTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.WeightTxt.Leave += new System.EventHandler(this.WeightTxt_Leave);
            // 
            // AddPerson
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(364, 277);
            this.Controls.Add(this.WeightTxt);
            this.Controls.Add(this.HeightTxt);
            this.Controls.Add(this.ValidationError);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.BirthDateTxt);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.EyesTxt);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.HairTxt);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.CityTxt);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.CountryTxt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.LastNameTxt);
            this.Controls.Add(this.GenderBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.FirstNameTxt);
            this.Controls.Add(this.label1);
            this.Name = "AddPerson";
            this.Text = "Add/Edit Person";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox FirstNameTxt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox GenderBox;
        private System.Windows.Forms.TextBox LastNameTxt;
        private System.Windows.Forms.TextBox CountryTxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox CityTxt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox EyesTxt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox HairTxt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.MaskedTextBox BirthDateTxt;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label ValidationError;
        private System.Windows.Forms.MaskedTextBox HeightTxt;
        private System.Windows.Forms.MaskedTextBox WeightTxt;


    }
}