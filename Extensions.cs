﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamShamsievDima
{
   public static class Extensions
    {
       public static bool IsEmpty(this string value)
       {
           return string.IsNullOrWhiteSpace(value);
       }
       public static DateTime ToDate(this string d)
       {
           return DateTime.ParseExact(d, "dd.MM.yyyy", null);
       
       }
    }
}
