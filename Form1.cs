﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExamShamsievDima
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// People binding list
        /// </summary>
        List<Person> _people;
        List<Person> _filteredPeople;
        BindingList<Person> _peopleBinding;
        BindingList<Gender> _genders = new BindingList<Gender>();
        BindingList<string> _countries = new BindingList<string>();
        BindingList<string> _cities = new BindingList<string>();
        BindingList<string> _hairColors = new BindingList<string>();
        BindingList<string> _eyesColors = new BindingList<string>();
        ZodiacComboBox _zodiacs;
        /// <summary>
        /// Selected hair color
        /// </summary>
        public string HairColor { get; set; }

        public Form1()
        {
            InitializeComponent();

            _zodiacs = ZodiacComboHost.Child as ZodiacComboBox;
            _people = new List<Person>();
            _people.Add( new Person
            {
                FirstName = "Dima",
                LastName = "Shamsiev",
                Gender = Gender.Male,
                BirthDate = Date( "16.05.1995" ),
                City = "Kharkiv",
                Country = "Ukraine",
                EyesColor = "Brown",
                HairColor = "Brown",
                Height = 187,
                Weght = 90
            } );

            _people.Add( new Person
            {
                FirstName = "Andrew",
                LastName = "Bagriy",
                Gender = Gender.Male,
                BirthDate = Date( "20.09.1995" ),
                City = "Kharkiv",
                Country = "Ukraine",
                EyesColor = "Blue",
                HairColor = "Brown",
                Height = 167,
                Weght = 65
            } );

            _people.Add( new Person
            {
                FirstName = "Oksana",
                LastName = "Shamsieva",
                Gender = Gender.Female,
                BirthDate = Date( "27.09.1989" ),
                City = "Kharkiv",
                Country = "Ukraine",
                EyesColor = "Brown",
                HairColor = "Brown",
                Height = 173,
                Weght = 60
            } );

            _people.Add( new Person
            {
                FirstName = "Helga",
                LastName = "Goncharenko",
                Gender = Gender.Female,
                BirthDate = Date( "27.09.1995" ),
                City = "Kharkiv",
                Country = "Ukraine",
                EyesColor = "Grey",
                HairColor = "Black",
                Height = 177,
                Weght = 60
            } );

            _people.Add( new Person
            {
                FirstName = "Vasya",
                LastName = "Sas",
                Gender = Gender.Male,
                BirthDate = Date( "10.01.1990" ),
                City = "Kharkiv",
                Country = "Ukraine",
                EyesColor = "Green",
                HairColor = "Blonde",
                Height = 167,
                Weght = 45
            } );

            GenderFilter.DataSource = _genders;
            CountryFilter.DataSource = _countries;
            HairColorFilter.DataSource = _hairColors;
            CityFilter.DataSource = _cities;
            EyesColorFilter.DataSource = _eyesColors;

            _filteredPeople = _people;
            _peopleBinding = new BindingList<Person>();
            BindFilters();
            Bind();
        }

        DateTime Date( string d )
        {
            return DateTime.ParseExact( d, "dd.MM.yyyy", null );

        }


        void BindFilters()
        {
            _zodiacs.Clear();
            _genders.Clear();
            _genders.Add( default( Gender ) );
            foreach ( var g in _filteredPeople.Select( p => p.Gender ).Distinct() )
            {
                _genders.Add( g );
            }

            _countries.Clear();
            _countries.Add( "- select country -" );
            foreach ( var c in _filteredPeople.Select( p => p.Country ).Distinct() )
            {
                _countries.Add( c );
            }

            _hairColors.Clear();
            _hairColors.Add( "- select hair color -" );
            foreach ( var h in _filteredPeople.Select( p => p.HairColor ).Distinct() )
            {
                _hairColors.Add( h );
            }

            _cities.Clear();
            _cities.Add( "- select city -" );
            foreach ( var c in _filteredPeople.Select( p => p.City ).Distinct() )
            {
                _cities.Add( c );
            }

            _eyesColors.Clear();
            _eyesColors.Add( "- select eyes color -" );
            foreach ( var e in _filteredPeople.Select( p => p.EyesColor ) )
            {
                _eyesColors.Add( e );
            }

            HeightMinTxt.Clear();
            HeightMaxTxt.Clear();

            WeightMaxTxt.Clear();
            WeightMinTxt.Clear();

        }



        void Bind()
        {
            _peopleBinding.Clear();
            _filteredPeople.ForEach( person => _peopleBinding.Add( person ) );
            PeopleGridView.DataSource = _peopleBinding;
        }

        private void ApplyBtn_Click( object sender, EventArgs e )
        {
            var hair = HairColorFilter.SelectedItem;
            var gender = GenderFilter.SelectedItem;
            var country = CountryFilter.SelectedItem;
            var city = CityFilter.SelectedItem;
            var eyes = EyesColorFilter.SelectedItem;

            _filteredPeople = _people.Where( p =>
               {
                   return
                       ( hair == HairColorFilter.Items[ 0 ] || ( string )hair == p.HairColor ) &&
                       ( gender == GenderFilter.Items[ 0 ] || ( Gender )gender == p.Gender ) &&
                       ( country == CountryFilter.Items[ 0 ] || ( string )country == p.Country ) &&
                       ( city == CityFilter.Items[ 0 ] || ( string )city == p.City ) &&
                       ( eyes == EyesColorFilter.Items[ 0 ] || ( string )eyes == p.EyesColor ) &&
                       CheckHeight( p ) &&
                       CheckWeight( p );

               } ).ToList();
            Bind();
        }

        /// <summary>
        /// Checks persons height
        /// </summary>
        /// <param name="p">Person</param>
        /// <returns></returns>
        private bool CheckHeight( Person p )
        {
            var minEmpty = string.IsNullOrWhiteSpace( HeightMinTxt.Text );
            var maxEmpty = string.IsNullOrWhiteSpace( HeightMaxTxt.Text );

            if ( minEmpty && maxEmpty )
                return true;

            int min = 0;
            int max = 0;
            Int32.TryParse( HeightMinTxt.Text, out min );
            Int32.TryParse( HeightMaxTxt.Text, out max );
            if ( min > max )
                return false;

            if ( minEmpty )
                return p.Height <= max;

            if ( maxEmpty )
                return p.Height >= min;

            return p.Height >= min && p.Height <= max;
        }

        /// <summary>
        /// Checks persons Weight
        /// </summary>
        /// <param name="p">Person</param>
        /// <returns></returns>
        private bool CheckWeight( Person p )
        {
            var minEmpty = string.IsNullOrWhiteSpace( WeightMinTxt.Text );
            var maxEmpty = string.IsNullOrWhiteSpace( WeightMaxTxt.Text );

            if ( minEmpty && maxEmpty )
                return true;

            int min = 0;
            int max = 0;
            Int32.TryParse( WeightMinTxt.Text, out min );
            Int32.TryParse( WeightMaxTxt.Text, out max );
            if ( min > max )
                return false;

            if ( minEmpty )
                return p.Weght <= max;

            if ( maxEmpty )
                return p.Weght >= min;

            return p.Weght >= min && p.Weght <= max;
        }

        private void ClearBtn_Click( object sender, EventArgs e )
        {
            _filteredPeople = _people;
            BindFilters();
            Bind();
        }

        private void button1_Click( object sender, EventArgs e )
        {
            var person = new Person();
            AddPerson wind = new AddPerson( person );
            wind.FormClosed += ( s, a ) =>
            {
                if ( !wind.Valide )
                    return;

                _people.Add( person );
                _peopleBinding.Add( person );

            };
            wind.ShowDialog();
        }

        private void button3_Click( object sender, EventArgs e )
        {
            if ( PeopleGridView.SelectedRows.Count == 0 )
            {
                NoPersonSelected();
                return;
            }

            var person = _filteredPeople[ PeopleGridView.SelectedRows[ 0 ].Index ];
            AddPerson wind = new AddPerson( person, true );
            wind.FormClosed += ( s, a ) =>
            {
                if ( !wind.Valide )
                    return;

                Bind();
            };
            wind.ShowDialog();
        }

        void NoPersonSelected()
        {
            MessageBox.Show( "Select a person first." );
        }

        private void button2_Click( object sender, EventArgs e )
        {
            if ( PeopleGridView.SelectedRows.Count == 0 )
            {
                NoPersonSelected();
                return;
            }

            var person = _filteredPeople[ PeopleGridView.SelectedRows[ 0 ].Index ];
            _people.Remove( person );
            _filteredPeople.Remove( person );
            Bind();
        }

        private void button5_Click( object sender, EventArgs e )
        {
            BindFilters();
        }
    }
}

