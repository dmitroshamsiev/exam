﻿namespace ExamShamsievDima
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.PeopleGridView = new System.Windows.Forms.DataGridView();
            this.HairColorFilter = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.ClearBtn = new System.Windows.Forms.Button();
            this.ApplyBtn = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.GenderFilter = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.EyesColorFilter = new System.Windows.Forms.ComboBox();
            this.CountryFilter = new System.Windows.Forms.ComboBox();
            this.CityFilter = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.HeightMinTxt = new System.Windows.Forms.MaskedTextBox();
            this.HeightMaxTxt = new System.Windows.Forms.MaskedTextBox();
            this.WeightMaxTxt = new System.Windows.Forms.MaskedTextBox();
            this.WeightMinTxt = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.firstNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.birthDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.genderDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.countryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.heightDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.weghtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hairColorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eyesColorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.personBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ZodiacComboHost = new System.Windows.Forms.Integration.ElementHost();
            this.zodiacComboBox1 = new ExamShamsievDima.ZodiacComboBox();
            this.form1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.PeopleGridView)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.personBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.form1BindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // PeopleGridView
            // 
            this.PeopleGridView.AllowUserToAddRows = false;
            this.PeopleGridView.AllowUserToDeleteRows = false;
            this.PeopleGridView.AutoGenerateColumns = false;
            this.PeopleGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PeopleGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.firstNameDataGridViewTextBoxColumn,
            this.lastNameDataGridViewTextBoxColumn,
            this.birthDateDataGridViewTextBoxColumn,
            this.genderDataGridViewTextBoxColumn,
            this.countryDataGridViewTextBoxColumn,
            this.cityDataGridViewTextBoxColumn,
            this.heightDataGridViewTextBoxColumn,
            this.weghtDataGridViewTextBoxColumn,
            this.hairColorDataGridViewTextBoxColumn,
            this.eyesColorDataGridViewTextBoxColumn});
            this.PeopleGridView.DataSource = this.personBindingSource;
            this.PeopleGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PeopleGridView.Location = new System.Drawing.Point(0, 0);
            this.PeopleGridView.MultiSelect = false;
            this.PeopleGridView.Name = "PeopleGridView";
            this.PeopleGridView.ReadOnly = true;
            this.PeopleGridView.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PeopleGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.PeopleGridView.Size = new System.Drawing.Size(1044, 485);
            this.PeopleGridView.TabIndex = 3;
            // 
            // HairColorFilter
            // 
            this.HairColorFilter.FormattingEnabled = true;
            this.HairColorFilter.Location = new System.Drawing.Point(6, 164);
            this.HairColorFilter.Name = "HairColorFilter";
            this.HairColorFilter.Size = new System.Drawing.Size(138, 21);
            this.HairColorFilter.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Add";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(87, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Delete";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.WeightMaxTxt);
            this.groupBox1.Controls.Add(this.WeightMinTxt);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.HeightMaxTxt);
            this.groupBox1.Controls.Add(this.HeightMinTxt);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.ZodiacComboHost);
            this.groupBox1.Controls.Add(this.ClearBtn);
            this.groupBox1.Controls.Add(this.ApplyBtn);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.GenderFilter);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.EyesColorFilter);
            this.groupBox1.Controls.Add(this.CountryFilter);
            this.groupBox1.Controls.Add(this.CityFilter);
            this.groupBox1.Controls.Add(this.HairColorFilter);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(162, 530);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 324);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(40, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "Zodiac";
            // 
            // ClearBtn
            // 
            this.ClearBtn.Location = new System.Drawing.Point(95, 397);
            this.ClearBtn.Name = "ClearBtn";
            this.ClearBtn.Size = new System.Drawing.Size(49, 23);
            this.ClearBtn.TabIndex = 21;
            this.ClearBtn.Text = "Clear";
            this.ClearBtn.UseVisualStyleBackColor = true;
            this.ClearBtn.Click += new System.EventHandler(this.ClearBtn_Click);
            // 
            // ApplyBtn
            // 
            this.ApplyBtn.Location = new System.Drawing.Point(40, 397);
            this.ApplyBtn.Name = "ApplyBtn";
            this.ApplyBtn.Size = new System.Drawing.Size(49, 23);
            this.ApplyBtn.TabIndex = 20;
            this.ApplyBtn.Text = "Apply";
            this.ApplyBtn.UseVisualStyleBackColor = true;
            this.ApplyBtn.Click += new System.EventHandler(this.ApplyBtn_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 234);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Height";
            // 
            // GenderFilter
            // 
            this.GenderFilter.FormattingEnabled = true;
            this.GenderFilter.Location = new System.Drawing.Point(6, 45);
            this.GenderFilter.Name = "GenderFilter";
            this.GenderFilter.Size = new System.Drawing.Size(138, 21);
            this.GenderFilter.TabIndex = 17;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(55, 253);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(10, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "-";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 188);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Eyes";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 148);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Hair";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "City";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Country";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Gender";
            // 
            // EyesColorFilter
            // 
            this.EyesColorFilter.FormattingEnabled = true;
            this.EyesColorFilter.Location = new System.Drawing.Point(6, 204);
            this.EyesColorFilter.Name = "EyesColorFilter";
            this.EyesColorFilter.Size = new System.Drawing.Size(138, 21);
            this.EyesColorFilter.TabIndex = 4;
            // 
            // CountryFilter
            // 
            this.CountryFilter.FormattingEnabled = true;
            this.CountryFilter.Location = new System.Drawing.Point(6, 84);
            this.CountryFilter.Name = "CountryFilter";
            this.CountryFilter.Size = new System.Drawing.Size(138, 21);
            this.CountryFilter.TabIndex = 3;
            // 
            // CityFilter
            // 
            this.CityFilter.FormattingEnabled = true;
            this.CityFilter.Location = new System.Drawing.Point(6, 124);
            this.CityFilter.Name = "CityFilter";
            this.CityFilter.Size = new System.Drawing.Size(138, 21);
            this.CityFilter.TabIndex = 2;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button4);
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(162, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1044, 45);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(249, 12);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 5;
            this.button4.Text = "Save";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(168, 12);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "Edit";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.PeopleGridView);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(162, 45);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1044, 485);
            this.panel1.TabIndex = 4;
            // 
            // button5
            // 
            this.button5.Image = ((System.Drawing.Image)(resources.GetObject("button5.Image")));
            this.button5.Location = new System.Drawing.Point(6, 397);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(26, 23);
            this.button5.TabIndex = 24;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(118, 253);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(21, 13);
            this.label11.TabIndex = 26;
            this.label11.Text = "cm";
            // 
            // HeightMinTxt
            // 
            this.HeightMinTxt.Location = new System.Drawing.Point(6, 250);
            this.HeightMinTxt.Mask = "000";
            this.HeightMinTxt.Name = "HeightMinTxt";
            this.HeightMinTxt.Size = new System.Drawing.Size(43, 20);
            this.HeightMinTxt.TabIndex = 27;
            this.HeightMinTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // HeightMaxTxt
            // 
            this.HeightMaxTxt.Location = new System.Drawing.Point(69, 250);
            this.HeightMaxTxt.Mask = "000";
            this.HeightMaxTxt.Name = "HeightMaxTxt";
            this.HeightMaxTxt.Size = new System.Drawing.Size(43, 20);
            this.HeightMaxTxt.TabIndex = 28;
            this.HeightMaxTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // WeightMaxTxt
            // 
            this.WeightMaxTxt.Location = new System.Drawing.Point(69, 291);
            this.WeightMaxTxt.Mask = "009";
            this.WeightMaxTxt.Name = "WeightMaxTxt";
            this.WeightMaxTxt.Size = new System.Drawing.Size(43, 20);
            this.WeightMaxTxt.TabIndex = 33;
            this.WeightMaxTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // WeightMinTxt
            // 
            this.WeightMinTxt.Location = new System.Drawing.Point(6, 291);
            this.WeightMinTxt.Mask = "009";
            this.WeightMinTxt.Name = "WeightMinTxt";
            this.WeightMinTxt.Size = new System.Drawing.Size(43, 20);
            this.WeightMinTxt.TabIndex = 32;
            this.WeightMinTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(118, 294);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(19, 13);
            this.label7.TabIndex = 31;
            this.label7.Text = "kg";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 275);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 13);
            this.label9.TabIndex = 30;
            this.label9.Text = "Weight";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(55, 294);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(10, 13);
            this.label12.TabIndex = 29;
            this.label12.Text = "-";
            // 
            // firstNameDataGridViewTextBoxColumn
            // 
            this.firstNameDataGridViewTextBoxColumn.DataPropertyName = "FirstName";
            this.firstNameDataGridViewTextBoxColumn.HeaderText = "First Name";
            this.firstNameDataGridViewTextBoxColumn.Name = "firstNameDataGridViewTextBoxColumn";
            this.firstNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lastNameDataGridViewTextBoxColumn
            // 
            this.lastNameDataGridViewTextBoxColumn.DataPropertyName = "LastName";
            this.lastNameDataGridViewTextBoxColumn.HeaderText = "LastName";
            this.lastNameDataGridViewTextBoxColumn.Name = "lastNameDataGridViewTextBoxColumn";
            this.lastNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // birthDateDataGridViewTextBoxColumn
            // 
            this.birthDateDataGridViewTextBoxColumn.DataPropertyName = "BirthDate";
            this.birthDateDataGridViewTextBoxColumn.HeaderText = "BirthDate";
            this.birthDateDataGridViewTextBoxColumn.Name = "birthDateDataGridViewTextBoxColumn";
            this.birthDateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // genderDataGridViewTextBoxColumn
            // 
            this.genderDataGridViewTextBoxColumn.DataPropertyName = "Gender";
            this.genderDataGridViewTextBoxColumn.HeaderText = "Gender";
            this.genderDataGridViewTextBoxColumn.Name = "genderDataGridViewTextBoxColumn";
            this.genderDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // countryDataGridViewTextBoxColumn
            // 
            this.countryDataGridViewTextBoxColumn.DataPropertyName = "Country";
            this.countryDataGridViewTextBoxColumn.HeaderText = "Country";
            this.countryDataGridViewTextBoxColumn.Name = "countryDataGridViewTextBoxColumn";
            this.countryDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cityDataGridViewTextBoxColumn
            // 
            this.cityDataGridViewTextBoxColumn.DataPropertyName = "City";
            this.cityDataGridViewTextBoxColumn.HeaderText = "City";
            this.cityDataGridViewTextBoxColumn.Name = "cityDataGridViewTextBoxColumn";
            this.cityDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // heightDataGridViewTextBoxColumn
            // 
            this.heightDataGridViewTextBoxColumn.DataPropertyName = "Height";
            this.heightDataGridViewTextBoxColumn.HeaderText = "Height";
            this.heightDataGridViewTextBoxColumn.Name = "heightDataGridViewTextBoxColumn";
            this.heightDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // weghtDataGridViewTextBoxColumn
            // 
            this.weghtDataGridViewTextBoxColumn.DataPropertyName = "Weght";
            this.weghtDataGridViewTextBoxColumn.HeaderText = "Weght";
            this.weghtDataGridViewTextBoxColumn.Name = "weghtDataGridViewTextBoxColumn";
            this.weghtDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // hairColorDataGridViewTextBoxColumn
            // 
            this.hairColorDataGridViewTextBoxColumn.DataPropertyName = "HairColor";
            this.hairColorDataGridViewTextBoxColumn.HeaderText = "HairColor";
            this.hairColorDataGridViewTextBoxColumn.Name = "hairColorDataGridViewTextBoxColumn";
            this.hairColorDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // eyesColorDataGridViewTextBoxColumn
            // 
            this.eyesColorDataGridViewTextBoxColumn.DataPropertyName = "EyesColor";
            this.eyesColorDataGridViewTextBoxColumn.HeaderText = "EyesColor";
            this.eyesColorDataGridViewTextBoxColumn.Name = "eyesColorDataGridViewTextBoxColumn";
            this.eyesColorDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // personBindingSource
            // 
            this.personBindingSource.DataSource = typeof(ExamShamsievDima.Person);
            // 
            // ZodiacComboHost
            // 
            this.ZodiacComboHost.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ZodiacComboHost.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ZodiacComboHost.Location = new System.Drawing.Point(6, 343);
            this.ZodiacComboHost.Name = "ZodiacComboHost";
            this.ZodiacComboHost.Size = new System.Drawing.Size(138, 36);
            this.ZodiacComboHost.TabIndex = 22;
            this.ZodiacComboHost.Text = "elementHost1";
            this.ZodiacComboHost.Child = this.zodiacComboBox1;
            // 
            // form1BindingSource
            // 
            this.form1BindingSource.DataSource = typeof(ExamShamsievDima.Form1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1206, 530);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.PeopleGridView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.personBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.form1BindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox HairColorFilter;
        private System.Windows.Forms.BindingSource form1BindingSource;
        private System.Windows.Forms.BindingSource personBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn firstNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn birthDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn genderDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn countryDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn heightDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn weghtDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn hairColorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn eyesColorDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox EyesColorFilter;
        private System.Windows.Forms.ComboBox CountryFilter;
        private System.Windows.Forms.ComboBox CityFilter;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView PeopleGridView;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox GenderFilter;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button ClearBtn;
        private System.Windows.Forms.Button ApplyBtn;
        private System.Windows.Forms.Integration.ElementHost ZodiacComboHost;
        private ZodiacComboBox zodiacComboBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.MaskedTextBox WeightMaxTxt;
        private System.Windows.Forms.MaskedTextBox WeightMinTxt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.MaskedTextBox HeightMaxTxt;
        private System.Windows.Forms.MaskedTextBox HeightMinTxt;
        private System.Windows.Forms.Label label11;
    }
}

